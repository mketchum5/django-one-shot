from django.urls import reverse_lazy
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.shortcuts import redirect

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todolist/list.html"
    context_object_name = "todolist_name"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name: str = "todolist/detail.html"
    context_object_name = "todolistdetail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todolist/create.html"
    fields = [
        "name",
    ]

    def form_valid(self, form):
        todo = form.save(commit=False)
        todo.owner = self.request.user
        todo.save()
        form.save_m2m()
        return redirect("todo_list_detail", pk=todo.id)

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todolist/edit.html"
    fields = [
        "name",
    ]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todolist/delete.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "item/create.html"
    fields = ["task", "due_date", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "item/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
