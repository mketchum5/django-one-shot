from django.contrib import admin
from todos.models import TodoList
from todos.models import TodoItem

# Register your models here.


admin.site.register(TodoList)
admin.site.register(TodoItem)
