from django.urls import path


from todos.views import TodoListListView, TodoListDetailView
from todos.views import TodoListCreateView, TodoListUpdateView
from todos.views import (
    TodoListDeleteView,
    TodoItemCreateView,
    TodoItemUpdateView,
)


urlpatterns = [
    path("", TodoListListView.as_view(), name="todo_list_list"),
    path(
        "todolist/<int:pk>/",
        TodoListDetailView.as_view(),
        name="todo_list_detail",
    ),
    path(
        "todolist/create/",
        TodoListCreateView.as_view(),
        name="todo_list_create",
    ),
    path(
        "todolist/<int:pk>/edit/",
        TodoListUpdateView.as_view(),
        name="todo_list_update",
    ),
    path(
        "todolist/<int:pk>/delete/",
        TodoListDeleteView.as_view(),
        name="todo_list_delete",
    ),
    path(
        "item/create/",
        TodoItemCreateView.as_view(),
        name="todo_item_create",
    ),
    path(
        "item/<int:pk>/edit/",
        TodoItemUpdateView.as_view(),
        name="todo_item_update",
    ),
]
